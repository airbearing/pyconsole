# Python serial console

## Contents

- [Installing dependencies](#dependencies)
- [Usage](#usage)
- [Docs](#docs)
- [References](#references)

## Installing dependencies

pySerial can be installed from PyPI:

```bash
pip install pyserial
```

## Usage

**Custom-made script** to receive input serial USB data and transmit commands:

```bash
python console.py <port_number>
```

where `port_number` refers to the port in use, like "COM4" or "tty1".

### Other useful commands

Launch default miniterm to receive data and transmit commands:

```bash
python -m serial.tools.miniterm <port_number>
```

List available ports:

```bash
python -m serial.tools.list_ports
```

## Documentation

- Docs have been generated using  [Doxygen](http://www.doxygen.nl/manual/starting.html).
- Configuration is defined in `Doxyfile`.

### Generating or editting the documentation

1. Install [Doxygen](http://www.doxygen.nl/manual/starting.html).
2. Install [Graphviz](https://www.graphviz.org/download/).
3. Go to Windows [environment variables](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/) and open the `PATH` variable.
4. Add Graphviz's `dot.exe` absolute path (something like `C:\Program Files (x86)\Graphviz2.38\bin`).
5. Modify comment blocks in code as desired.
6. Edit `Doxyfile` configuration file if desired.
7. Run:

```bash
doxygen Doxyfile
```

## References

- [Pyserial repo](https://github.com/pyserial/pyserial)
- [Pyserial docs](https://pyserial.readthedocs.io/en/latest/index.html)
- [Pyserial miniterm docs](https://pyserial.readthedocs.io/en/latest/tools.html#module-serial.tools.miniterm)