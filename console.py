#!/usr/bin/env python
## @package console
#  Receive incoming data and transmit commands through Serial USB.
#
#  Developed for IDR - UPM (2019).
#  It is intended to provide coms with Station's Feather M0 in a 
#  nominal environment and coms with Platform's Teensy for debug


# -------------------------------------------------------
# Modules
# -------------------------------------------------------
import serial
import sys
import os
import time

# -------------------------------------------------------
# Classes
# -------------------------------------------------------

## Auxiliary color class
#
#  Format prompted text with colors and style.
class c:
    ## header (pink color)
    HD = '\033[95m'
    ## blue color
    BL = '\033[94m' 
    ## green color
    GR = '\033[92m' 
    ## yellow color (warning)
    YL = '\033[93m' 
    ## red color (fail)
    RD = '\033[91m' 
    ## end of format
    END = '\033[0m' 
    ## bold weight
    B = '\033[1m'   
    ## underline
    U = '\033[4m'   


# -------------------------------------------------------
# Functions
# -------------------------------------------------------

## Clears text from terminal
def clearTerminal():
    os.system('cls' if os.name == 'nt' else 'clear')

## Prints formatted banners into terminal
#  @param section The target to be prompted
def prompt(section: str = 'title'):
    if section == 'title':
        print(c.HD + '╒═══════════════════════════════════════════╕' + c.END)
        print(c.HD + '│             AIR BEARING CONSOLE           │' + c.END)
        print(c.HD + '╞═══════════════════════════════════════════╡' + c.END)
        print(c.HD + '│                  IDR - UPM                │' + c.END)
        print(c.HD + '│                                           │' + c.END)
        print(c.HD + '│             David Criado Pernia           │' + c.END)
        print(c.HD + '╘═══════════════════════════════════════════╛' + c.END)
    elif section == 'transmit':
        print('\r\n  Sending command...')
        print('─────────────────────────────────────────────')
    elif section == 'resume':
        print('\r\n  Resuming...')
        print('─────────────────────────────────────────────')
    elif section == 'preamble':
        print('\r\n  Prompting incoming serial data...')
        print('  Type Ctrl+C to send a command')
        print('─────────────────────────────────────────────\r\n ')
    elif section == 'cmd':
        print('╒═══════════════════════════════════════════╕')
        print('│                COMMAND INPUT              │')
        print('╞═══════════════════════════════════════════╡')
        print('│Enter your command below                   │')
        print('│                                           │')
        print('│ '+c.GR+'"x,y,z"'+c.END+'     Send triad to Platform        │')
        print('│                                           │')
        print('│ '+c.GR+'h'+c.END+' | '+c.GR+'help'+c.END +'    Get help on custom commands   │')
        print('│ '+c.GR+'r'+c.END+' | '+c.GR+'resume'+c.END+'  Resume prompting data         │')
        print('│ '+c.GR+'e'+c.END+' | '+c.GR+'exit'+c.END +'    Leave the application         │')
        print('╘═══════════════════════════════════════════╛')
    elif section == 'help':
        print('╒═══════════════════════════════════════════╕')
        print('│                   HELP                    │')
        print('╞═══════════════════════════════════════════╡')
        print('│ '+c.GR+'y'+c.END+' | '+c.GR+'yes'+c.END +'     Reply to Platform (60,1,0)    │')
        print('│ '+c.GR+'n'+c.END+' | '+c.GR+'no'+c.END +'      Reply to Platform (60,2,0)    │')
        print('╘═══════════════════════════════════════════╛')

## Open serial port
#
#  Tries to open specified port and sets Serial object
def openPort():
    (port, baudrate) = parseArgs()
    try:
        ser = serial.Serial(port, baudrate, timeout=0)
        print('\r\n  Connected to serial port {}'.format(port))
        print('  Baudrate: {}'.format(baudrate))
        print('─────────────────────────────────────────────')
        return ser
    except serial.SerialException:   
        print(c.RD + '┌───────────────────────────────────────────┐' + c.END)
        print(c.RD + '  Serial port {} not available'.format(port)   + c.END)
        print(c.RD + '└───────────────────────────────────────────┘' + c.END)
        return False

## Parse additional arguments when script starts.
#  @param port The port to set the Serial connection (default: COM4)
#  @param baudrate The rate of data transfer (default: 115200)
#  @return Incoming args else default port and baudrate
def parseArgs(port: str = 'COM4', baudrate: int = 115200):
    if len(sys.argv) >= 2 and isinstance(sys.argv[1], str):
        port = sys.argv[1]
    elif len(sys.argv) >= 3 and isinstance(sys.argv[2], int):
        baudrate = sys.argv[2]
    return (port, baudrate)

## Handle user commands
#  @param ser Selected serial connection
def readCommand(ser: serial.Serial):
    try:
        clearTerminal()
        prompt('cmd')
        literal = input(">> ")
        if not literal or literal.lower() in ['resume', 'r']:
            clearTerminal()
            prompt('resume')
            prompt('preamble')
            return False
        elif literal.lower() in ['exit','e']:
            exit(ser)
        elif literal.lower() in ['help', 'h']:
            clearTerminal()
            prompt('help')
            prompt('resume')
            prompt('preamble')
            return False
        else:
            clearTerminal()
            prompt('transmit')
            prompt('preamble')
            cmd = interpretCommand(literal)
        ser.write(str.encode(cmd + '\r\n'))
        ser.flush()
    except KeyboardInterrupt:
        exit(ser)

## Translates user-friendly commands into tuples
#  @param command user-friendly string
#  @return translated csv tuple
def interpretCommand(command: str):
    if command.lower() in ['yes', 'y']:
        return '60,1,0'
    elif command.lower() in ['no', 'n']:
        return '60,2,0'
    else:
        return command

## Exits script
#  @param ser Selected serial connection
def exit(ser: serial.Serial):
    ser.close()
    sys.exit(1)

## Reads serial data
#  @param ser Selected serial connection
def readSerial(ser: serial.Serial):
    line = ser.readline().decode().strip('\r\n')
    if len(line) > 0:
        print(line)

## Handles incoming telemetry and outcoming commands.
def manageSerial():
    ser = openPort()
    if not ser:
        return False
    prompt('preamble')
    while(1):
        try:
            readSerial(ser)
        except serial.SerialException:
            try:
                print("[!SER]\tAttempting reconnection in {} seconds...".format(timeout))
                time.sleep(timeout)
                clearTerminal()
                manageSerial() # Recursive attempt
            except KeyboardInterrupt:
                exit(ser)
        except KeyboardInterrupt:
            readCommand(ser)

## Main script function
def main():
    clearTerminal()
    prompt()
    manageSerial()


# -------------------------------------------------------
# Main program
# -------------------------------------------------------
if __name__ == '__main__':
    try:
        ## Timeout between openning attempts
        timeout = 5
        main()
    except (SystemExit):
        print('Exiting script...')
        sys.exit(1)
